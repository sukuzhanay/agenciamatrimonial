/***************************************************************************
*  PROGRAMA:     AGENCIA MATRIMONIAL / SPANISH-TINDER.
*
*  DESCRIPCION : Programa que lee desde archivo de BINARIO los CANDIDATOS a
*                encontrar pareja 
*
*  PROFESOR :    JAVIER SANCHEZ SORIANO
*
*  ALUMNO:       Christian Vladimir Sukuzhanay Arévalo
*         
* **************************************************************************/

#include<iostream>
#include<fstream>
#include<cctype>
#include<iomanip>
#include <cstdlib>

using namespace std;

//***************************************************************
//                   CLASE CANDIDATO
//****************************************************************


class candidato
{
	int dni;
	char nombre[20];
    char apellido[20];
	int edad;
	char sexo;
    char prefsexual;
    int caracter;
    int gmusicales;
    int glectura;
    int glibre;
    int gsex; 
public:
	void crear_candidato();	//Crea candidato
	void mostrar_candidato() const;	//Muestra candidato
	void modify();	//Modifica candidato
	void reporte() const;//Muestra candidato en formato tabla
	int retdni() const;	//Funcion que devuelve DNI, NIE, Pasaporte
	char retsexo() const;	//Funcion que devuelve el  sexo del candidato
}; // Termina clase candidato

void candidato::crear_candidato()
{
   int control=0;
   int opcion=0;
   cout<<"\n Ingrese edad del Candidato ? : ";
   cin>>edad;
   if ((edad<16)||(edad >100))
      {
       cout<<"\n No se permite dar de alta a MENORES de 16 ni mayores a 100, por imperativo legal";
       return;
      }
   
   
   
   
   for(;;){
      char letras[] = "TRWAGMYFPDXBNJZSQVHLCKE";
      char letra; 
      int len;
      cout<<"\n Introduzca el numero de tu DNI (sin letra): ";
      cin>>dni;
      cout<<"\n Introduzca la letra del DNI: ";
      cin.ignore();
      letra=getchar();
            
      cout.fill('*');
      
      letra=toupper(letra);
      if(letra==letras[dni%23]){
          cout<<"\nDNI--> es correcto";
          break;
      }else{
          cout<<"\nDNI--> NO es correcto";
      }
   }


   
   do 
   {
      cout<<"\n\nIngrese el nombre del Candidat@  : ";
      cin>>nombre;
      int i=0;
      for(i = 0; nombre[i]; i++)
      {
         if(isalpha(nombre[i])==0){
            cout<<"\n\t\t"<<nombre<< " ---> No es un nombre valido ** ";
            break;
         }else 
            control=1;           
      }
   }while (control==0);
   cout<<nombre<<"\n\t ---> VALIDADO \n";
    
    do
   {   
      cout<<"\n\t Ingrese apellido ? ";
      cin>>apellido;
      int i=0;
      for (i=0; apellido[i];i++)
      {
         if(isalpha(apellido[i])==0){
            cout<<"\n\t\t"<<apellido<< " ---> No es un apellido valido ** \n";
            control=0;
            break;
         } else
            control=1;
      }
   }while(control==0);
   cout<<"\n\t\t ---> VALIDADO \n";

   for(;;)
{
cout<<"\n\nIngrese el SEXO del Candidat@ \n";
cout<<"\n [ H ]= Hombre [ M ]= Mujer [ T ]= Transexual : ";
cin>>sexo;
sexo=toupper(sexo); 
if (sexo=='H' || sexo=='M'|| sexo=='T'){
break;
}
cout <<"\n Salvo que seas HEMAFRODITA, el sexo no es correcto !!";
}


for(;;)
{
   cout<<"\n\nIngrese las preferencias SEXUALES del Candidat@ \n\n";
   cout<<"\n [ H ]= Hombre [ M ]= Mujer [ T ]= Transexual : ";
   cin>>prefsexual;
   prefsexual=toupper(prefsexual); 
   if (prefsexual=='H' || prefsexual=='M'|| prefsexual=='T'){
   break;
}
cout <<"\n Salvo que seas HERMAFRODITA, el sexo no es correcto !!";
}

for(;;)
{
cout<<"\n A continuación se le va a pasar un test, elija la opción que mejor le defina \n\n";
cout<<"\t\t SU CARÁCTER \n";
cout <<"\n [ 1 ] Timido     [ 2 ] Extrovertido     [ 3 ] Romantico    [ 4 ] Afable \n";
cout<< "\n Elegija su Opcion:  : ";
cin>>caracter; 
if (caracter==1|| caracter==2|| caracter==3 || caracter==4){
break;
}

}


for(;;)
{
cout<<"\t\t GUSTOS MUSICALES \n";
cout <<"\n [ 1 ] Clasico   [ 2 ] Moderno  [ 3 ] No me gusta la música \n";
cout<< "\n Elegija su Opcion:  : ";
cin>>gmusicales; 
if (gmusicales==1|| gmusicales==2|| gmusicales==3){
break;
}

}


for(;;)
{

cout<<"\t\t GUSTOS DE LECTURA \n";
cout <<"\n [ 1 ] Novela     [ 2 ] Poesia     [ 3 ] Ensayo    [ 4 ] No me gusta leer \n";
cout<< "\n Elegija su Opcion:  : ";
cin>>glectura; 
if (glectura==1|| glectura==2|| glectura==3 || glectura==4){
break;
}

}     


for(;;)
{

cout<<"\t\t GUSTOS DE TIEMPO LIBRE \n";
cout <<"\n [ 1 ] Deporte     [ 2 ] Actividades culturales     [ 3 ] Fiesta    [ 4 ] Quedarme en casa \n";
cout<< "\n Elegija su Opcion:  : ";   
cin>>glibre; 
if (glibre==1|| glibre==2|| glibre==3 || glibre==4){
break;
}

}        


}     
    
 

void candidato::mostrar_candidato() const
{
	cout<<"\n DNI / NIE / PASAPORTE : "<<dni;
	cout<<"\n NOMBRE : "<<nombre;
    cout<<"\n APELIDO : "<<apellido;
	cout<<"\n SEXO"<<sexo;
	cout<<"\n EDAD"<<edad;
    cout<<"\n TIPO DE CARACTER: "<<caracter;
	cout<<"\n MUSICA PREFERIDA"<<gmusicales;
    cout<<"\n LITERATURA FAVORITA : "<<glectura;
	cout<<"\n TIEMPO LIBRE FAVORITO :"<<glibre;
	cout<<"\n PREFERENCIA SEXUAL :"<<prefsexual;  
}


void candidato::modify()
{

   
   int control=0;
   int opcion=0;
   cout<<"\n Ingrese edad del Candidato ? : ";
   cin>>edad;
   if ((edad<16)||(edad >100))
      {
       cout<<"\n No se permite dar de alta a MENORES de 16 ni mayores a 100, por imperativo legal";
       return;
      };
      
    cout<<"\n Ingrese el Dni / Nie / Pasaporte :";
	cin>>dni;
   do
   {
         cout<<"\n\nIngrese el nombre del Candidat@  : ";
         cin>>nombre;
         int i=0;
         for(i = 0; nombre[i]; i++)
            {
               if(isalpha(nombre[i])==0){
                  cout<<"\n\t\t"<<nombre<< " ---> No es un nombre valido ** ";
                  break;
               }else 
                  control=1;           
            }
    }while (control==0);
    cout<<nombre<<"\n\t ---> VALIDADO \n";
    
    do
      {   
         cout<<"\n\t Ingrese apellido ? ";
         cin>>apellido;
         int i=0;
            for (i=0; apellido[i];i++)
            {
               if(isalpha(apellido[i])==0){
                  cout<<"\n\t\t"<<apellido<< " ---> No es un apellido valido ** \n";
                  control=0;
                  break;
               } else
                  control=1;
            }
      }while(control==0);

      cout<<"\n\t\t ---> VALIDADO \n";

             
      for(;;)
      {
       cout<<"\n\nIngrese el SEXO del Candidat@ \n";
       cout<<"\n [ H ]= Hombre [ M ]= Mujer [ T ]= Transexual : ";
       cin>>sexo;
       sexo=toupper(sexo); 
       if (sexo=='H' || sexo=='M'|| sexo=='T'){
           break;
       }
       cout <<"\n Salvo que seas HEMAFRODITA, el sexo no es correcto !!";
      }
    
      
      for(;;)
      {
       cout<<"\n\nIngrese las preferencias SEXUALES del Candidat@ \n\n";
       cout<<"\n [ H ]= Hombre [ M ]= Mujer [ T ]= Transexual : ";
       cin>>prefsexual;
       prefsexual=toupper(prefsexual); 
       if (prefsexual=='H' || prefsexual=='M'|| prefsexual=='T'){
           break;
       }
       cout <<"\n Salvo que seas HEMAFRODITA, el sexo no es correcto !!";
      }
      
for(;;)
      {
       cout<<"\n A continuación se le va a pasar un test, elija la opción que mejor le defina \n\n";
      cout<<"\t\t SU CARÁCTER \n";
      cout <<"\n [ 1 ] Timido     [ 2 ] Extrovertido     [ 3 ] Romantico    [ 4 ] Afable \n";
      cout<< "\n Elegija su Opcion:  : ";
       cin>>caracter; 
       if (caracter==1|| caracter==2|| caracter==3 || caracter==4){
           break;
       }
       
      }


    for(;;)
      {
      cout<<"\t\t GUSTOS MUSICALES \n";
      cout <<"\n [ 1 ] Clasico   [ 2 ] Moderno  [ 3 ] No me gusta la música \n";
      cout<< "\n Elegija su Opcion:  : ";
       cin>>gmusicales; 
       if (gmusicales==1|| gmusicales==2|| gmusicales==3){
           break;
       }
       
      }


 for(;;)
      {
       
      cout<<"\t\t GUSTOS DE LECTURA \n";
      cout <<"\n [ 1 ] Novela     [ 2 ] Poesia     [ 3 ] Ensayo    [ 4 ] No me gusta leer \n";
      cout<< "\n Elegija su Opcion:  : ";
       cin>>glectura; 
       if (glectura==1|| glectura==2|| glectura==3 || glectura==4){
           break;
       }
       
      }     
      
      
   for(;;)
      {
       
      cout<<"\t\t GUSTOS DE TIEMPO LIBRE \n";
      cout <<"\n [ 1 ] Deporte     [ 2 ] Actividades culturales     [ 3 ] Fiesta    [ 4 ] Quedarme en casa \n";
      cout<< "\n Elegija su Opcion:  : ";   
       cin>>glibre; 
       if (glibre==1|| glibre==2|| glibre==3 || glibre==4){
           break;
       }
       
      }         
   
}

	
	
void candidato::reporte() const
{
	cout<<dni<<setw(3)<<" "<<nombre<<setw(7)<<" "<<apellido<<setw(7)<<" "<<sexo<<setw(5)<<edad<<setw(5)<<" "<<prefsexual<<setw(5)<<caracter<<" "<<setw(5)<<" "<<gmusicales<<" "<<endl;
    
}

	
int candidato::retdni() const
{
	return dni;
}



char candidato::retsexo() const
{
	return sexo;
}


//***************************************************************
//    	Bloque de declaracion de funciones
//****************************************************************
void grabar_candidato();	//Graba candidato en archivo binario
void mostrar_especifico(int);//Muestra candidato especifico
void modify_candidato(int);	//Modifica candidato existente en archivo
void eliminar_candidato(int);//Elimina candidato
void mostrar_candidatos();//Muestra todos los candidatos
void intro();//Intro de Universidad Europea, pinta y colorea

//***************************************************************
//    	Programa Principal
//****************************************************************


int main()
{
	char ch;
	int num;
	intro();
	do
	{
		system("clear");
		cout<<"\n\n\n\t_____________MENU_______________";
		cout<<"\n\n\t [ 1 ] INSERTAR CANDIDAT@";
        cout<<"\n\n\t [ 2 ] MODIFICAR DATOS DE UN CANDIDAT@";
        cout<<"\n\n\t [ 3 ] ELIMINAR CANDIDAT@";
		cout<<"\n\n\t [ 4 ] BUSCAR COMPATIBILIDADES";
		cout<<"\n\n\t [ 5 ] LISTAR TODOS LOS CANDIDAT@S";
		cout<<"\n\n\t [ 6 ] SALIR";
		cout<<"\n\n\t Seleccione su opcion (1-8) ";
		cin>>ch;
		system("clear");
		switch(ch)
		{
		case '1':
			grabar_candidato();
			break;
		case '4': 
			cout<<"\n\n\t Ingrese el DNI / NIE / PASAPORTE : "; cin>>num;
			mostrar_especifico(num);
			break;
		case '5':
			mostrar_candidatos();
			break;
		case '3':
			cout<<"\n\n\t Ingrese el DNI / NIE / PASAPORTE : "; cin>>num;
			eliminar_candidato(num);
			break;
		 case '2':
			cout<<"\n\n\t Ingrese el DNI / NIE / PASAPORTE : "; cin>>num;
			modify_candidato(num);
			break;
		 case '6':
			cout<<"\n\n\t Gracias por usar __Spanish Tinder___";
			break;
		 default :cout<<"\a";
		}
		cin.ignore();
		cin.get();
	}while(ch!='6');
	return 0;
}


//***************************************************************
//    	FUNCION QUE ESCRIBE EN ARCHIVO BINARIO EL OBJETO
//****************************************************************

void grabar_candidato()
{
	candidato ac;
	ofstream outFile;
	outFile.open("candidato.dat",ios::binary|ios::app);
	ac.crear_candidato();
	outFile.write(reinterpret_cast<char *> (&ac), sizeof(candidato));
	outFile.close();
    cout<<"\n\n\n Candidato creado con EXITO.....";
}

//***************************************************************
//    FUNCION QUE LEE DESDE ARCHIVO BINARIO OBJETO ESPECIFICO
//****************************************************************

void mostrar_especifico(int n)
{
	candidato ac;
	bool flag=false;
	ifstream inFile;
	inFile.open("candidato.dat",ios::binary);
	if(!inFile)
	{
		cout<<"NO se puede abrir el archivo !! Pulse cualquier tecla...";
		return;
	}

    	while(inFile.read(reinterpret_cast<char *> (&ac), sizeof(candidato)))
	{
		if(ac.retdni()==n)
		{
			ac.mostrar_candidato();
			flag=true;
		}
	}
	inFile.close();
	if(flag==false)
		cout<<"\n\n NO EXISTE CANDIDAT@";
}


//***************************************************************
// FUNCION QUE MODIFICA UN OBJETO GUARDADO EN ARCHIVO BINARIO
//****************************************************************

void modify_candidato(int n)
{
	bool found=false;
	candidato ac;
	fstream File;
	File.open("candidato.dat",ios::binary|ios::in|ios::out);
	if(!File)
	{
		cout<<"El archivo BINARIO no se puede abrir, esta vacio o error en escritura !! Pulse cualquier tecla...";
		return;
	}
	while(!File.eof() && found==false)
	{
		File.read(reinterpret_cast<char *> (&ac), sizeof(ac));
		if(ac.retdni()==n)
		{
			ac.mostrar_candidato();
			cout<<"\n\n Introduzca la nueva informacion del Candidat@"<<endl;
			ac.modify();
			int pos=(-1)*static_cast<int>(sizeof(candidato));
			File.seekp(pos,ios::cur);
			File.write(reinterpret_cast<char *> (&ac), sizeof(candidato));
			cout<<"\n\n\t Candidato actualizado";
			found=true;
		  }
	}
	File.close();
	if(found==false)
		cout<<"\n\n Candidato NO encontrado ";
}

//***************************************************************
//    FUNCION QUE ELIMINA UN OBJETO DEL ARCHIVO BINARIO
//****************************************************************


void eliminar_candidato(int n)
{
	candidato ac;
	ifstream inFile;
	ofstream outFile;
	inFile.open("candidato.dat",ios::binary);
	if(!inFile)
	{
		cout<<"NO se puede abrir el archivo !! Pulse cualquier tecla...";
		return;
	}
	outFile.open("Temp.dat",ios::binary);
	inFile.seekg(0,ios::beg);
	while(inFile.read(reinterpret_cast<char *> (&ac), sizeof(candidato)))
	{
		if(ac.retdni()!=n)
		{
			outFile.write(reinterpret_cast<char *> (&ac), sizeof(candidato));
		}
	}
	inFile.close();
	outFile.close();
	remove("candidato.dat");
	rename("Temp.dat","candidato.dat");
	cout<<"\n\n\tContacto eliminado ..";
}

//***************************************************************
//    FUNCIOn QUE MUESTRA TODOS LOS CANDIDATOS

//****************************************************************

void mostrar_candidatos()
{
	candidato ac;
	ifstream inFile;
	inFile.open("candidato.dat",ios::binary);
	if(!inFile)
	{
		cout<<"NO se puede abrir el archivo !! Pulse cualquier tecla...";
		return;
	}
	cout<<"\n\n\t\t BASE DE DATOS DE CLIENTES DE << SPANISH TINDER >>\n\n";
	cout<<"====================================================================\n";
	cout<<"DNI NOMBRE  APELLIDO       SEXO EDAD PSEX CARACTER MUSIC LITER       \n";
	cout<<"====================================================================\n";
	while(inFile.read(reinterpret_cast<char *> (&ac), sizeof(candidato)))
	{
		ac.reporte();
	}
	inFile.close();
}

//***************************************************************
//    	FUNCION INTRO
//****************************************************************

// Funcion que pinta y colorea a la Universidad Europea
void intro()
{
 system("clear");
            cout<<"\n\n\t\t_____________________________________________________________________"<<endl;
            cout<<"\t\t                                                                    "<<endl;
            cout<<"\t\t UNIVERSIDAD EUROPEA - ESCUELA DE ARQUITECTURA, INGENIERIA Y DISEÑO"<<endl;
            cout<<"\t\t_____________________________________________________________________"<<endl<<endl;
            cout<<"\n\n\n\t\t\t\tProfesor : Javier Sanchez Soriano. "<<endl<<endl<<endl;
            cout<<"\n\t\t\t\t   Alumnos: Christian Sukuzhanay\n"<<endl<<endl<<endl;
            cout<<"\t\t\t    ________S P A N I S H   T I N D E R ________"<<endl<<endl;
            cout<<"\n\n\n\t\t\t Encuentra el Amor a lo Iberico "<<endl<<endl<<endl;
            cout<<"\t\t** Si no encuentra el amor en 30 dias, le devolvemos el dinero     **\n ";
            cout<<"\t\t** y le obsequiamos una cita previa con CORPORACION DERMOESTETICA  **"<<endl<<endl<<endl;
            cout<<"\t\t\t\t\t  < < A V I S O  > > "<<endl<<endl;
            cout<<"\t\t\tCumpliendo la legislacion Espanola no se discrimina por sexo \n\n\n";
            cout<<"\t\t\t           Pulse cualquier tecla para ir al menu _ : \n";
            cin.get();
    
}

//***************************************************************
//    			FIN
//***************************************************************